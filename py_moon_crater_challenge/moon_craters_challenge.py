"""
generate moon crater queries
"""

import sys
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import argparse
import random
from matplotlib import gridspec
from matplotlib.offsetbox import OffsetImage,AnnotationBbox

from gtts import gTTS

from logger import logger

class MoonCraterQuery:
    """
    Query moon craters 

    Attributes:
    """

    def __init__(self, in_dirpath):
        """
        """
        self.disableEventHandling = False
        self.img_dirpath = in_dirpath
        self.img_fpath = os.path.join(in_dirpath, "moon.png")
        self.queries_fpath = os.path.join(in_dirpath, "qatable.txt")
        self.load_queries(in_fpath=self.queries_fpath)

        self.n = len(self.queries)
        self.iteration = -1
        self.idx_already_played = []

        self.score = 0
        self.success = 0

    def load_queries(self, in_fpath):
        """
        """
        self.queries = []
        with open(in_fpath, 'r') as f:
            for line in f:
                if "#" not in line and len(line)>0:
                    split_line = line.split("\t")
                    qu_d = {}
                    qu_d["query"] = split_line[0]
                    qu_d["h"] =  float(split_line[1])
                    qu_d["v"] = float(split_line[2].strip("\n"))
                    
                    self.queries.append(qu_d)

    def query_all(self):
        """
        """
        self.n_queries = self.n

        logger.info("N queries: {}".format(self.n_queries))

        for k in range(self.n_queries):
            self.query_next()
            

    def show_query(self, title_override="", show_answer=False, enable_speech=False):
        mpl.rcParams['toolbar'] = 'None'


        self.fig = plt.figure( "Moon craters challenge", figsize=(14,10), facecolor='black')
        self.fig.suptitle("{}".format(title_override), size=24, y=0.95, color="w", alpha=0.8)
        
        query_str = self.queries[self.iteration]["query"]
        self.answer = {"h":self.queries[self.iteration]["h"], "v":self.queries[self.iteration]["v"]}
        self.tol = 35

        if enable_speech and not show_answer:
            text_tts = "Trouver le cratère {}".format(query_str)
            language = "fr"
            speech = gTTS(text=text_tts, lang=language, slow=True)
            speech.save("tmp_tts.mp3")
            os.system("play tmp_tts.mp3")

        self.canvas = self.fig.canvas
        self.axes = []

        remove_n_rows = 0

        rows=1
        columns=2

        gs = gridspec.GridSpec(rows, columns, width_ratios=[1, 2.5])

        self.axes = []
        for ks in range(columns):
            ax = plt.subplot(gs[ks])
            self.axes.append(ax)

        #self.axes.append(self.fig.add_subplot(rows, columns, 1))
        self.axes[0].plot([0, 100], [0, 100], "k")
        self.axes[0].plot([160, 160], [-10, 120], "w", alpha=0.1)
        self.axes[0].text(-40, 100, "SCORE: {:.0f}".format(self.score), color="w", fontsize=20, alpha=0.8)
        
        if show_answer:
            if self.success:
                _color = "g"
                self.axes[0].text(-40, 90, "BRAVO !".format(), color=_color, fontsize=20, alpha=0.7)


                if enable_speech:
                    text_tts = "Gagné !".format()
                    language = "fr"
                    speech = gTTS(text=text_tts, lang=language, slow=True)
                    speech.save("tmp_tts.mp3")
                    os.system("play tmp_tts.mp3")
                
            else:
                _color = "r"
                self.axes[0].text(-40, 90, "FAUX !".format(), color=_color, fontsize=20, alpha=0.7)

                if enable_speech:
                    text_tts = "Perdu !".format()
                    language = "fr"
                    speech = gTTS(text=text_tts, lang=language, slow=True)
                    speech.save("tmp_tts.mp3")
                    os.system("play tmp_tts.mp3")
                    
            self.axes[0].text(-40, 75, "Réponse - cratère : \n".format(query_str), color=_color, fontsize=20, alpha=0.5)
        else:
            self.axes[0].text(-40, 75, "Clique sur le cratère : \n".format(query_str), color="w", fontsize=20, alpha=0.5)
        self.axes[0].text(-40, 70, "{}".format(query_str), color="w", fontsize=26)
        self.axes[0].text(-40, 20, "q: quit", color="w", fontsize=16, alpha=0.4)
        self.axes[0].get_xaxis().set_visible(False)
        self.axes[0].get_yaxis().set_visible(False)
        #self.axes[0].text(2000, 100, "XXXXXXXXXXXXXXXXX")
        self.axes[0].set_facecolor((.0, 0., 0.))

        #show image at the right
        img_i=mpimg.imread(self.img_fpath)
        #self.axes.append(self.fig.add_subplot(rows, columns, 2))
        self.axes[1].imshow(img_i[remove_n_rows:, :])

        if show_answer:
            #add answer location
            self.axes[1].plot([self.answer["h"]-self.tol, self.answer["h"]+self.tol], [self.answer["v"]+self.tol, self.answer["v"]+self.tol], _color)
            self.axes[1].plot([self.answer["h"]+self.tol, self.answer["h"]-self.tol], [self.answer["v"]-self.tol, self.answer["v"]-self.tol], _color)
            self.axes[1].plot([self.answer["h"]-self.tol, self.answer["h"]-self.tol], [self.answer["v"]-self.tol, self.answer["v"]+self.tol], _color)
            self.axes[1].plot([self.answer["h"]+self.tol, self.answer["h"]+self.tol], [self.answer["v"]+self.tol, self.answer["v"]-self.tol], _color)
        self.axes[1].get_xaxis().set_visible(False)
        self.axes[1].get_yaxis().set_visible(False)
        #self.axes[1].set_title("Please click on...")
        

        #plt.tight_layout()
        plt.subplots_adjust(top=0.9) 
        if not self.disableEventHandling:
            self.startEventHandling()
        else:
            self.fig.show()
            plt.show()

    def startEventHandling(self):
        print("start event handling: ...")
        print("\t Click on the map to select a crater")
        print("\t")
        self.coords = []
        # Call click func
        cid1 = self.fig.canvas.mpl_connect('button_press_event', self.onclick)
        cid2 = self.fig.canvas.mpl_connect('key_press_event', self.on_key_press)
        self.enablepointpicking = True
        
        plt.show()
        self.fig.canvas.mpl_disconnect(cid2)
        
    ##callback for point picking event    
    def onclick(self, event):
        global ix, iy
        if event.button == 1: #1=left click, 3=rightclick
            if True:        
                ix, iy = event.xdata, event.ydata
                print("x = {:.2f}, y = {:.4e}".format(float(ix), float(iy)))
                print("ans_h = {:.2f}, ans_v = {:.4e}".format(float(self.answer["h"]), float(self.answer["v"])))   
                if np.sqrt((self.answer["h"]-ix)**2+(self.answer["v"]-iy)**2)<self.tol:
                    self.score += 1
                    self.success = 1
                    logger.info("Answer found !! BRAVO, new score : {}".format(self.score))
                else:
                    self.score += 0
                    self.success = 0
                plt.close()
                self.coords.append((ix, iy))
                self.refresh()
        if self.enablepointpicking:
            print("INFO: there are {} points in store".format(len(self.coords)))


    def on_key_press(self, event):
        print("key pressed = {}".format(event.key))
        if event.key == 'q':
            exit()

    def refresh(self):
        self.canvas.draw()

    def query_next(self):
        """
        """

        if 0:
            self.iteration += 1
        else:
            self.iteration = -1
            while (self.iteration==-1) or self.iteration in self.idx_already_played:
                self.iteration = int(random.random()*self.n)
                logger.info("Randomly selected iteration={}".format(self.iteration))

        logger.info("Iteration finally selected = {}".format(self.iteration))

        self.idx_already_played.append(self.iteration)
        title_str = ""
        self.show_query(title_override=title_str)
        self.show_query(title_override=title_str, show_answer=True)
        


def listFiles(f1Directory, inclusion_string=None):
    """
    Scans input directories and build files list whose shape is Nx1
    """
    fileList = []
	
    for i,file in enumerate(sorted(os.listdir(f1Directory))):
        if inclusion_string is None:
            incl_valid = True
        else:
            logger.debug("Inclusion string is {}".format(inclusion_string))
            logger.debug("str(file) string is {}".format(str(file)))
            incl_valid = (inclusion_string in str(file))
        if (file.endswith(".png")) and incl_valid: 
            a1 = str(file)
            fileList.append(a1)
    return fileList


if __name__ == "__main__":
    """
    """
    logger.info("Starting Moon craters challenge now.")

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, description="""
                            Processing Moon craters challenge:
                            """)

    parser.add_argument('-i', 
                        action="store", 
                        dest="dir_path", 
                        default="./data", 
                        help="path to the input queries directory")

    args = parser.parse_args()

    if os.path.isdir(args.dir_path):
        fullinputdirpath = os.path.abspath(args.dir_path)
    else:
        logger.error("input directory does not exist. aborting")
        exit()

        
    mcq = MoonCraterQuery(in_dirpath=fullinputdirpath) 
    mcq.query_all()
