"""
Note. The selection of the log level threshold (ex. INFO or DEBUG) is done via code for now (see below). It could be passed as an argument in the future
"""
import os
import logging
import datetime

# logging setup
log_handlers = []
log_handlers.append(logging.StreamHandler())

#TODO: define a path to the log file to be populated with logs
logPath=""
if (logPath is not "") and (not os.path.isdir(logPath)):
    os.makedirs(logPath)
    date_str = datetime.datetime.now().strftime('%Y%m%d_%H-%M-%S')
    logfileName="log_{}".format(date_str)
    log_handlers.append(logging.FileHandler("{0}/{1}.log".format(logPath, logfileName)))

logging.basicConfig(
    #level=logging.DEBUG, #choose debug or higher log levels  (info, warning, error) to be printed
    level=logging.INFO, #choose info or higher log levels (warning, error) to be printed
    #level=logging.ERROR, #choose info or higher log levels (warning, error) to be printed
    format="%(asctime)s [%(levelname)-5.5s] [%(filename)-30s:%(lineno)-5s] %(message)s",
    handlers=log_handlers)
logger = logging.getLogger("cgr")

#set some imported modules log levels
logging.getLogger("matplotlib").setLevel(logging.WARNING)